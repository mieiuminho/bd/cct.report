var mongo = require("mongodb");
var url = "mongodb://root:dockermongo@localhost:27017/";

function drop_collection(dbo, collection) {
  dbo.dropCollection(collection, function(err, res) {
    if (err) throw err;
    if (res) console.log("Collection " + collection + " deleted");
  });
}

mongo.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("cct");
  drop_collection(dbo, "atletas");
  drop_collection(dbo, "consultas");
  drop_collection(dbo, "equipamentos");
  drop_collection(dbo, "especialidades");
  drop_collection(dbo, "exames");
  drop_collection(dbo, "hospitais");
  drop_collection(dbo, "medicos");
  drop_collection(dbo, "modalidades");
  drop_collection(dbo, "organizadores");
  drop_collection(dbo, "provas");
  db.close();
});

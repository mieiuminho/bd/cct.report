var mongo = require("mongodb");
var url = "mongodb://root:dockermongo@localhost:27017/";

function add_to_collection(dbo, collection) {
  var objects = require("./data/" + collection + ".json");
  dbo.collection(collection).insertMany(objects, function(err, res) {
    if (err) throw err;
    console.log(
      "Number of documents inserted in " + collection + ": " + res.insertedCount
    );
  });
}

mongo.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("cct");
  add_to_collection(dbo, "atletas");
  add_to_collection(dbo, "consultas");
  add_to_collection(dbo, "equipamentos");
  add_to_collection(dbo, "especialidades");
  add_to_collection(dbo, "exames");
  add_to_collection(dbo, "hospitais");
  add_to_collection(dbo, "medicos");
  add_to_collection(dbo, "modalidades");
  add_to_collection(dbo, "organizadores");
  add_to_collection(dbo, "provas");
  db.close();
});

const mongo = require("mongodb");
const url = "mongodb://root:dockermongo@localhost:27017/";

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

// 1. Consultar as consultas agendadas para um atleta.
async function query1(nome_atleta, hora_limite_inferior, hora_limite_superior) {
  mongo.connect(url, async function(err, db) {
    if (err) throw err;
    var dbo = db.db("cct");
    var atleta = dbo
      .collection("atletas")
      .findOne({ name: nome_atleta }, { _id: 0, id: 1 });
    dbo
      .collection("consultas")
      .find({
        ["idAtleta"]: atleta.id,
        ["hora_inicio"]: { $gt: hora_limite_inferior },
        ["hora_fim"]: { $lt: hora_limite_superior }
      })
      .toArray(function(err, result) {
        if (err) throw err;
        console.log(result);
        db.close();
      });
  });
}

// 2. Consultar o historial clínico de um atleta, fornenedo o seu nome.
async function query2(nome_atleta) {
  mongo.connect(url, async function(err, db) {
    if (err) throw err;
    var dbo = db.db("cct");
    var atleta = await dbo
      .collection("atletas")
      .findOne({ name: nome_atleta }, { _id: 0, id: 1 });
    var appointments = new Array();
    dbo
      .collection("consultas")
      .find({ idAtleta: atleta.id }, { _id: 0, id: 1 })
      .toArray(function(err, result) {
        if (err) throw err;
        result.forEach(row => {
          appointments.push(row.id);
          dbo
            .collection("exames")
            .find({ ["consultas"]: row.id })
            .toArray(function(err, result) {
              if (err) throw err;
              console.log(result);
            });
        });
        db.close();
      });
  });
}

// 3. Consultar o stock dos equipamentos num hospital, fornecendo o seu nome
async function query3(nome_hospital) {
  mongo.connect(url, async function(err, db) {
    if (err) throw err;
    var dbo = db.db("cct");
    var hospital = await dbo
      .collection("hospitais")
      .findOne({ name: nome_hospital }, { _id: 0, id: 1 });
    dbo
      .collection("equipamentos")
      .find(
        { ["hospitais.hospital"]: hospital.id },
        {
          _id: 0,
          designacao: 1,
          "hospitais.stock": { $elemMatch: { hospital: hospital.id } }
        }
      )
      .toArray(function(err, result) {
        if (err) throw err;
        result.forEach(row => {
          console.log(row);
        });
        db.close();
      });
  });
}

// 4. Aceder aos exames necessários à inscrição numa prova
async function query4(nome_prova) {
  mongo.connect(url, async function(err, db) {
    if (err) throw err;
    var dbo = db.db("cct");
    var prova = await dbo
      .collection("provas")
      .findOne({ ["name"]: nome_prova }, { _id: 0, id: 1 });
    dbo
      .collection("provas")
      .find({ ["id"]: prova.id }, { _id: 0, name: 1, exames: 1 })
      .toArray(function(err, result) {
        if (err) throw err;
        result.forEach(row => {
          console.log(row.name);
          console.log(row.exames);
        });
        db.close();
      });
  });
}

// 5. Aceder ao organizador de uma prova fornecendo o nome
async function query5(nome_prova) {
  mongo.connect(url, async function(err, db) {
    if (err) throw err;
    var dbo = db.db("cct");
    var prova = await dbo
      .collection("provas")
      .findOne({ ["name"]: nome_prova });
    dbo
      .collection("organizadores")
      .findOne({ ["provas"]: prova.id }, function(err, result) {
        if (err) throw err;
        console.log(result.name);
        db.close();
      });
  });
}

//TODO 6. Aceder ao número de consultas que cada atleta teve, por especialidade médica.
async function getConsultasPorEspecialidade(dbo, id_atleta) {
  let result = {};
  let medicos = [];

  const consultas = await dbo
    .collection("consultas")
    .find({ ["idAtleta"]: id_atleta })
    .toArray();

  consultas.forEach(async function(consulta) {
    medicos.push(consulta.idMedico);
  });

  await asyncForEach(medicos, async id_medico => {
    const especialidade = await dbo
      .collection("especialidades")
      .findOne({ ["medicos"]: id_medico });
    if (result.hasOwnProperty(especialidade.id)) {
      result[especialidade.nome] = result[especialidade.nome] + 1;
    } else {
      result[especialidade.nome] = 1;
    }
  });

  console.log(result);
}

// 7. Obter  a  lista  de  provas  em  que  um  atleta  se  pode  inscrever  (tendo  em conta a modalidade que pratica)
async function getFuturasProvas(dbo, id_atleta) {
  const atleta = await dbo.collection("atletas").findOne({ ["id"]: id_atleta });
  const modalidade = await dbo
    .collection("modalidades")
    .findOne({ ["atletas"]: id_atleta });

  console.log("Atleta: " + atleta.name);
  console.log("Modalidade: " + modalidade.nome);

  const current_date = new Date();

  modalidade.provas.forEach(async function(prova_id) {
    const prova = await dbo.collection("provas").findOne({ ["id"]: prova_id });
    if (new Date(prova.data_inicio).getTime() > current_date.getTime()) {
      console.log(prova.name + " " + prova.data_inicio);
    }
  });
}

// 8. Obter o número de atletas, por modalidade, inscritos no sistema.
async function getNumAtletasModalidade(dbo, id_modalidade) {
  const modalidade = await dbo
    .collection("modalidades")
    .findOne({ ["id"]: id_modalidade });

  console.log(
    "Nº de Atletas na modalidade " +
      id_modalidade +
      ": " +
      modalidade.atletas.length
  );
}

// 9. Obter a lista de provas de uma determinada modalidade.
async function getProvasSameModalidadeDatas(dbo, id_modalidade) {
  const modalidade = await dbo
    .collection("modalidades")
    .findOne({ ["id"]: id_modalidade });

  console.log("Provas de " + modalidade.nome + ":");

  modalidade.provas.forEach(async function(prova_id) {
    let prova = await dbo.collection("provas").findOne({ ["id"]: prova_id });
    console.log(prova.name);
  });
}

mongo.connect(
  url,
  {
    poolSize: 10,
    bufferMaxEntries: 0,
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  async function(err, db) {
    if (err) throw err;
    let dbo = db.db("cct");
    await getConsultasPorEspecialidade(dbo, 4);
    await getFuturasProvas(dbo, 1);
    await getNumAtletasModalidade(dbo, 3);
    await getProvasSameModalidadeDatas(dbo, 6);
    db.close();
  }
);
